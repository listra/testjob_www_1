<?php

require('vendor/autoload.php');

use App\GetRabbit;
use App\RequestFactory;
use App\ConstantsConf;

chdir(dirname(__DIR__));

try {

    if ($_SERVER["CONTENT_TYPE"] != 'application/json') {
        throw new \Exception('Content type must be: application/json');
    }

    $request = RequestFactory::fromGlobals('', file_get_contents('php://input'));

    (new GetRabbit())->send(ConstantsConf::QUEUE, $request->getParsedBody());

    echo "Ok \n";

} catch (Throwable $e) {
    echo "Captured Throwable: " . $e->getMessage() . PHP_EOL;
}
