<?php

require('vendor/autoload.php');

use App\GetRabbit;
use App\ConstantsConf;

chdir(dirname(__DIR__));

(new GetRabbit())->receive(ConstantsConf::QUEUE);
