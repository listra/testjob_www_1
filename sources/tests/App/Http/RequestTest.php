<?php

namespace Tests\App;

use App\RequestFactory;
use PHPUnit\Framework\TestCase;

class RequestTest extends TestCase
{

    public function testIndex(): void
    {
        $request = RequestFactory::fromGlobals(['GET'], '{"JsonData":"Text"}');

        $json = json_decode($request->getParsedBody());

        self::assertEquals(
            $json->JsonData,
            "Text"
        );
    }

}
