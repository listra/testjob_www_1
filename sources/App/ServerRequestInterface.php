<?php

namespace App;

/**
 * Interface ServerRequestInterface
 * @package App
 */
interface ServerRequestInterface
{
    /**
     * @return mixed
     */
    public function getQueryParams();

    /**
     * @param array $query
     * @return mixed
     */
    public function setQueryParams(array $query);

    /**
     * @param $data
     * @return mixed
     */
    public function setParsedBody($data);

    /**
     * @return mixed
     */
    public function getParsedBody();

}
