<?php

namespace App;

use PhpAmqpLib\Message\AMQPMessage;

/**
 * Class GetRabbit
 * @package App
 */
class GetRabbit extends GetRabbitConnect
{
    /**
     * @param $queue
     * @param $data
     */
    public function send($queue, $data)
    {
        try {

            $this->channel->queue_declare($queue, false, false, false, false);

            $this->channel->basic_publish(
                new AMQPMessage($data), '', $queue);

            $this->closeConnectChanel();

        } catch (\Throwable $e) {
            echo $e->getMessage() . PHP_EOL;
        }
    }

    /**
     * @param $queue
     */
    public function receive($queue)
    {
        print_r($queue);
        $this->channel->queue_declare($queue, false, false, false, false);

        echo " Start Waiting for messages. \n";

        $log = new Logger();

        $callback = function ($msg) use ($log, $queue) {

            $log->info(" [x] Received ($queue) " . $msg->body);
            echo " [x] Received ($queue) " . $msg->body, "\n";
        };

        $this->channel->basic_consume($queue, '', false, true, false, false, $callback);

        while (count($this->channel->callbacks)) {
            try {
                 $this->channel->wait();
            } catch (\ErrorException $e) {
                echo $e->getMessage() . PHP_EOL;
            }
        }
        $this->closeConnectChanel();

    }

    protected function closeConnectChanel()
    {
        $this->channel->close();

        try {
            $this->connect->close();
        } catch (\Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }
    }

}
