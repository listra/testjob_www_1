<?php

namespace App;


/**
 * Class Request
 * @package App
 */
class Request implements ServerRequestInterface
{

    /**
     * @var array
     */
    private $queryParams;
    /**
     * @var null
     */
    private $parsedBody;

    /**
     * Request constructor.
     * @param array $queryParams
     * @param null $parsedBody
     */
    public function __construct(array $queryParams = [], $parsedBody = null)
    {
        $this->queryParams = $queryParams;
        $this->parsedBody = $parsedBody;
    }

    /**
     * @return array
     */
    public function getQueryParams(): array
    {
        return $this->queryParams;
    }

    /**
     * @param array $query
     * @return $this
     */
    public function setQueryParams(array $query): self
    {
        $new = clone $this;
        $new->queryParams = $query;
        return $new;
    }

    /**
     * @param $data
     * @return Request
     */
    public function setParsedBody($data)
    {
        $new = clone $this;
        $new->parsedBody = $data;
        return $new;
    }

    /**
     * @return null
     */
    public function getParsedBody()
    {
        return $this->parsedBody;
    }

}
