<?php

namespace App;

/**
 * Class ConstantsConf
 * @package App
 */
class ConstantsConf
{
    const QUEUE = "TestQueue";
    const QUEUE_HOST = "rabbitmq";
    const QUEUE_PORT = "5672";
    const QUEUE_USER = "rabbituser";
    const QUEUE_PASSWORD = "rabbitpassword";

    const LOG_PATH = '/var/www/html/logs/log';
}
