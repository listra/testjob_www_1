<?php

namespace App;

/**
 * Class RequestFactory
 * @package App
 */
class RequestFactory
{
    /**
     * @param null $query
     * @param null $body
     * @return Request
     */
    public static function fromGlobals($query = null, $body = null): Request
    {
        return (new Request())
            ->setQueryParams($query ?: $_GET)
            ->setParsedBody($body ?: $_POST);
    }
}
