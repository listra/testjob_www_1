<?php

namespace App;

use Exception;

/**
 * Class Logger
 * @package App
 */
class Logger
{

    /**
     * @var string
     */
    protected $log_file;

    /**
     * @var
     */
    protected $file;

    /**
     * @var array
     */
    protected $params;

    /**
     * @var array
     */
    protected $options = ['dateFormat' => 'd-M-Y H:i:s'];


    /**
     * Logger constructor.
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        $this->log_file = ConstantsConf::LOG_PATH . '-' . $time = date('d-M-Y') . '.txt';;
        $this->params = array_merge($this->options, $params);

        try {
            if (!file_exists($this->log_file)) {
                fopen($this->log_file, 'w') or exit("Can't create $this->log_file!");
            }
            if (!is_writable($this->log_file)) {
                throw new Exception("ERROR: Unable to write to file!", 1);
            }
        } catch (\Throwable $e) {
            echo $e->getMessage() . PHP_EOL;
        }
    }

    /**
     * @param $message
     */
    public function info($message)
    {
        $this->writeLog($message, 'INFO');
    }

    /**
     * @param $message
     */
    public function warning($message)
    {
        $this->writeLog($message, 'WARNING');
    }

    /**
     * @param $message
     */
    public function error($message)
    {
        $this->writeLog($message, 'ERROR');
    }

    /**
     * @param $message
     * @param $severity
     */
    public function writeLog($message, $severity)
    {

        if (!is_resource($this->file)) {
            $this->openLog();
        }

        $path = $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        $time = date($this->params['dateFormat']);
        fwrite($this->file, "[$time] [$path] : [$severity] - $message" . PHP_EOL);
    }

    /**
     *
     */
    private function openLog()
    {
        $openFile = $this->log_file;
        $this->file = fopen($openFile, 'a') or exit("Can't open $openFile!");
    }

    /**
     *
     */
    public function __destruct()
    {
        if ($this->file) {
            fclose($this->file);
        }
    }

}
