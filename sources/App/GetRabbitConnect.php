<?php

namespace App;

use PhpAmqpLib\Connection\AMQPStreamConnection;

/**
 * Class GetRabbitConnect
 * @package App
 */
abstract class GetRabbitConnect
{
    protected $connect;
    protected $channel;

    /**
     * GetRabbitConnect constructor.
     */
    public function __construct()
    {
        if ($this->connect == null) {
            $this->connect = new AMQPStreamConnection(
                ConstantsConf::QUEUE_HOST,
                ConstantsConf::QUEUE_PORT,
                ConstantsConf::QUEUE_USER,
                ConstantsConf::QUEUE_PASSWORD);
        }
        $this->channel = $this->connect->channel();
    }

}
